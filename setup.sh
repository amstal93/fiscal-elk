#!/usr/bin/env bash
dir="$(git rev-parse --show-toplevel)"

pipenv install --dev

echo "${dir}/run_tests.sh" > "${dir}/.git/hooks/pre-commit"

chmod +x "${dir}/run_tests.sh"
chmod +x "${dir}/.git/hooks/pre-commit"
