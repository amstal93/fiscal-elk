resource "digitalocean_domain" "fiscal-elk" {
  name       = "${terraform.workspace == "default" ? "" : format("%s.", terraform.workspace)}${var.domain_name}"
  ip_address = "${digitalocean_floating_ip.fiscal-elk.ip_address}"
}
