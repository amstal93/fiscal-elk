#!/usr/bin/env bash
build_tmp_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

elasticsearch_url='http://localhost:9200'
kibana_url='http://localhost:5601'
kibana_dashboard_dir="${build_tmp_dir}/kibana_dashboards"
fiscal_data_dir="/var/lib/fiscal"
index="fiscal"
RETRIES=60

echo "[POPULATE ELK] Starting kibana and elasticsearch"
systemctl start elasticsearch
systemctl start kibana

echo "[POPULATE ELK] Wait for elasticsearch"
for ((i = 1; i <= RETRIES; i += 1)); do
  curl "${elasticsearch_url}" --silent && break
  sleep "${i}"
done

echo "[POPULATE ELK] Wait for kibana"
for ((i = 1; i <= RETRIES; i += 1)); do
  curl "${kibana_url}" --silent && break
  sleep "${i}"
done

echo "[POPULATE ELK] Downloading FI\$Cal data" \
  && echo -e "from fiscal_elk import load_fiscal\nload_fiscal.main('${fiscal_data_dir}')" \
    | sudo --user=fiscal_updater python \
  && echo "[POPULATE ELK] Downloading City of Sacramento data" \
  && sudo --user=fiscal_updater /usr/bin/fiscal_elk/download_cityofsacramento_data.sh "${fiscal_data_dir}"

echo "[POPULATE ELK] Add dashboards"
for filename in `ls "${kibana_dashboard_dir}/"`; do
  /usr/sbin/fiscal_elk/kibana/add_dashboard.sh "${kibana_dashboard_dir}/${filename}" "${kibana_url}" > /dev/null
done

echo "[POPULATE ELK] Add index patterns" \
  && /usr/sbin/fiscal_elk/kibana/add_index_pattern.sh "cityofsacramento_business_operation_tax_information" "application_date" \
  && /usr/sbin/fiscal_elk/kibana/add_index_pattern.sh "cityofsacramento_approved_budget" \
  && /usr/sbin/fiscal_elk/kibana/add_index_pattern.sh "cityofsacramento_council_office_expenses" "journal_date" \
  && /usr/sbin/fiscal_elk/kibana/add_index_pattern.sh "cityofsacramento_checks" "check_date" \
  && /usr/sbin/fiscal_elk/kibana/add_index_pattern.sh "cityofsacramento_purchase_orders_to_date" "po_date"


echo "[POPULATE ELK] Set default index pattern" \
  && curl --request POST "${kibana_url}/api/kibana/settings" \
    --header "content-type: application/json" \
    --header "kbn-xsrf: true" \
    --data "{\"changes\":{\"defaultIndex\":\"888d7980-e6f1-11e8-9a72-1bd5c01ee117\"}}"  \
    --silent \
    --show-error

echo "[POPULATE ELK] Opt out of telemetry" \
  && curl --request POST "${kibana_url}/api/telemetry/v1/optIn" \
    --header "content-type: application/json" \
    --header "kbn-xsrf: true" \
    --data "{\"enabled\":false}" > /dev/null \
    --silent \
    --show-error
