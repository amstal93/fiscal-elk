#!/usr/bin/env bash

username_part="[A-z0-9_.@-]*"
domain_part="@[A-z0-9.-]+[.][A-z]{2,}$"
filter_re="^fiscal_"
email_re="(?<= )${username_part}${domain_part}"
username_re="^${username_part}(?=${domain_part})"

function log() {
  local msg="${1}"
  local lvl="${2:-debug}"

  echo "[UPDATE USERS] ${lvl}: ${msg}"
  logger -p "user.${lvl}" -t update_users "${msg}"
}

function remove_user() {
  local username="${1}"

  id -u "${username}" &> /dev/null
  if [[ "${?}" -eq 0 ]]; then
    log "Removing ${username}"
    deluser --remove-home --quiet "${username}"
    if [[ "${?}" -ne 0 ]]; then log "Failed to delete user '${username}'" error; fi
  fi
}

function add_user() {
  local email="${1}"
  local username=`echo ${email} | grep -Po "${username_re}"`
  local keys_file="/home/${username}/.ssh/authorized_keys"

  id -u "${username}" &> /dev/null
  if [[ "${?}" -ne 0 ]]; then
    log "Adding user '${username}'"
    adduser \
      --quiet \
      --disabled-password \
      --shell "/bin/bash" \
      --gecos ",,,,${email}" \
      "${username}"
    if [[ "${?}" -ne 0 ]]; then log "Failed to add user '${username}'" error; fi

    addgroup "${username}" fiscal > /dev/null
    if [[ "${?}" -ne 0 ]]; then log "Failed to add group '${username}'" error; fi
  fi

  log "Setting ssh keys for user '${username}'"
  cat "${tmp_dir}/${email}" | tee "${keys_file}" > /dev/null
}

tmp_dir=`sudo --user=ssh_updater mktemp -d`
cd "${tmp_dir}"
# Get SSH users as ssh_updater, not root
echo -e "from fiscal_elk import get_ssh_keys\nget_ssh_keys.main('${tmp_dir}', '${filter_re}', '${email_re}')" \
  | sudo --user=ssh_updater /usr/bin/python

current_fiscal_users=`find ${tmp_dir} -type f -printf "%f\n"`

# Ensure the script is run with root privileges
if [[ "${UID}" -ne 0 ]]; then
  log "Must have root privileges to update users"
  exit
fi

# If user exists on the system but not in the list of users pulled from Digital Ocean, remove the user account
while read -r username; do
  if [[ "${current_fiscal_users}" != *"${username}${domain_part}"* ]]; then
    remove_user "${username}"
  fi
done <<< `grep "${domain_part}" /etc/passwd | cut -d: -f1`

# Ensure a user account exists for all users and the ssh keys are copied over
while read -r username; do
  add_user "${username}"
done <<< "${current_fiscal_users}"

rm -rf "${tmp_dir}"
