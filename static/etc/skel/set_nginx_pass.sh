#!/usr/bin/env bash

username="${1:-`whoami`}"
password=$(diceware)
hashed_password=`echo "${password}" | openssl passwd -apr1 -stdin`
password_file="/etc/nginx/htpasswd.users"

set_failed=1
update_failed=2

# Check that the password file exists and whether it already contains an entry for the username
if [[ -f "${password_file}" ]] && grep "^${username}:" "${password_file}" > /dev/null; then
  sudo sed -i "s/^${username}:.*\$/${username}:${hashed_password}/g" "${password_file}" || exit "${update_failed}"
else
  echo "${username}:${hashed_password}" | sudo tee -a "${password_file}" > /dev/null || exit "${set_failed}"
fi

echo "Set nginx password for '${username}' to '${password}'"
